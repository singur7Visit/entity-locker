package com.company.locker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;

import static org.junit.Assert.*;

public class EntityLockerTest {
    private ExecutorService executor;
    private EntityLocker<Integer> locker;


    @Before
    public void setUp() {
        this.executor = Executors.newFixedThreadPool(2);
        this.locker = new EntityLocker<>();
    }

    @After
    public void tearDown() {
        executor.shutdownNow();
    }

    @Test
    public void LockUnlockOneThreadTest() throws Exception {
        int id = 1;
        locker.lock(id);
        locker.unlock(id);
    }

    @Test
    public void lockUnlockTwoThreadsTest() throws Exception {
        int id = 1;
        Runnable runnable = () -> {
            try {
                locker.lock(id);
                locker.unlock(id);
            } catch (InterruptedException e) {
                assert false : e.getMessage();
            }
        };

        Future task1 = executor.submit(runnable);
        Future task2 = executor.submit(runnable);

        task1.get();
        task2.get();
    }

    @Test(expected = TimeoutException.class)
    public void lockTest() throws Exception {
        int id = 1;

        locker.lock(id);
        Future task1 = executor.submit(() -> {
            try {
                locker.lock(id);
            } catch (InterruptedException ignore) {
            }
            locker.unlock(id);
        });

        try {
            task1.get(10, TimeUnit.MILLISECONDS);
        } finally {
            task1.cancel(true);
            locker.unlock(id);
        }
    }

    @Test
    public void lockTwoEntitiesTest() throws Exception {
        int id1 = 1;
        int id2 = 2;

        locker.lock(id1);

        Future task1 = executor.submit(() -> {
            try {
                locker.lock(id2);
                locker.unlock(id2);
            } catch (InterruptedException e) {
                assert false : e.getMessage();
            }
        });

        task1.get(50, TimeUnit.MILLISECONDS);

        locker.unlock(id1);
    }

    @Test
    public void lockTimeoutTest() throws Exception {
        int id = 1;

        locker.lock(id);
        try {
            Future<Boolean> task1 = executor.submit(() -> {
                try {
                    boolean locked = locker.tryLock(id, 10, TimeUnit.MILLISECONDS);
                    if (locked) locker.unlock(id);
                    return locked;
                } catch (InterruptedException e) {
                    assert false : e.getMessage();
                    return true;
                }
            });
            assertFalse(task1.get());
        } catch (InterruptedException e) {
            assert false : e.getMessage();
        } finally {
            locker.unlock(id);
        }
    }
}