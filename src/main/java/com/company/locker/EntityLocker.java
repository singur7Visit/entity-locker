package com.company.locker;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * EntityLocker implementation based on IdentityHashMap.
 *
 * @param <ID> Entity ID type
 */
public class EntityLocker<ID> {

    private final Map<ID, ReentrantLock> locks = new IdentityHashMap<>();

    /**
     * Lock the entity ID
     *
     * @param entityId entity ID to lock, must not be null
     * @throws InterruptedException     locking was interrupted
     * @throws NullPointerException in case entity id is null
     */
    public void lock(ID entityId) throws InterruptedException {
        Objects.requireNonNull(entityId, "Entity id must not be null");
        synchronized (entityId) {
            ReentrantLock lock = getLock(entityId);
            lock.lockInterruptibly();
        }

    }

    /**
     * Lock the entity ID with the given timeout
     *
     * @param entityId entity ID to lock, must not be null
     * @throws InterruptedException     locking was interrupted
     * @throws NullPointerException in case entity id is null
     * @return true if lock was acquired
     */

    public boolean tryLock(ID entityId, long timeout, TimeUnit timeUnit) throws InterruptedException {
        Objects.requireNonNull(entityId, "Entity id must not be null");
        synchronized (entityId) {
            ReentrantLock lock = getLock(entityId);
            return lock.tryLock(timeout, timeUnit);
        }

    }

    private ReentrantLock getLock(ID entityId) {
        return locks.computeIfAbsent(entityId, id -> new ReentrantLock());
    }

    /**
     * Unlock the entity ID if it is locked, do nothing otherwise
     *
     * @param entityId entity ID to unlock, must not be null
     * @throws NullPointerException in case entity id is null
     */
    public void unlock(ID entityId) {
        Objects.requireNonNull(entityId, "Entity id must not be null");
        ReentrantLock lock = locks.get(entityId);
        if (lock != null) {
            lock.unlock();
            synchronized (entityId) {
                if (!lock.isLocked() && !lock.hasQueuedThreads()) {
                    locks.remove(entityId);
                }
            }
        }
    }
}

